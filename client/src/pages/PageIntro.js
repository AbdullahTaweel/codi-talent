// modules imports
import React from 'react';
import ReactGA from 'react-ga';
// files imports
import './PageIntro.css';

class PageIntro extends React.Component {

    componentDidMount() {
        ReactGA.initialize('UA-90768506-2');
        ReactGA.pageview('Intro Page');
    }

    render() {
        return (
            <div className="intro">
                <h1 className="item">Recruit Codi Talents!</h1><br />
                <h3 className="item">Full Stack</h3><br />
                <a href="/home" className="button"> Codi Alumni</a>
            </div>
        )
    }
}

export default PageIntro
