// modules imports
import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom'
// files imports
import './index.css';
import Router from './Router'

ReactDOM.render((
    <BrowserRouter>
        <Router/>
    </BrowserRouter>
), document.getElementById('root'));
