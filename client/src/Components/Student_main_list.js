import  student_list from '../data/students.json'
// function that used in the student_main_list

const randomStudentList = (a) => { // Fisher-Yates shuffle, no side effects
  // make the student list with random arrange
  var i = a.length, t, j;
  a = a.slice()
  if (i === 0) { return [] }
  while (--i) {
    t = a[i]
    a[i] = a[j = ~~(Math.random() * (i + 1))]
    a[j] = t
  }
  return a
}

const student_main_list = randomStudentList(student_list) //make the list rendered randomly

export default student_main_list;
